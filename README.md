# object-detection-dataset

Dataset for candidate's pratical part, where we have a small test for implementing a object detector in a agricultural environment.

The task is to build a object detector for detecting grape clusters in grape vines.

Inside the dataset folder you have a 100 images and corresponding annotation files in the YOLO format:
```
CLASS CX CY W H
```

The center of the bounding box (CX,CY), the width (W) and height(H), are float values, normalizing the coordinates by the image dimensions.

To get the absolute position, use (2048 c_x, 1365 c_y). The bounding box dimensions are given by W and H, are also normalized by the image size.
